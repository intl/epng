#include <stdio.h>
#include <stdlib.h>
#include "epng.h"


uint8_t stack[0x1000] = {0};
uint16_t stack_pointer = 0;

void stack_push(uint8_t data)
	{
	printf("%c",data);
	stack[stack_pointer++] = data;
	}

uint8_t stack_pop(void)
	{
	return stack[stack_pointer--];
	}

void lcd_write(png_color_t* color)
	{
	printf("#%x%x%x;",color->R,color->G,color->B);
	}

uint32_t get_bits(uint32_t from, uint32_t to, uint8_t *in)
	{
	uint32_t out=0;
	if(from>to)
		{
		//���� ����
		for(from++; from>=(to+1); --from)
			{
			out <<= 1;
			out |= ((*(in+((from-1)>>3))) >>  ((from-1)&0x7))&1;
			}

		}
	else
		{
		//���� �����
		for(; from<=to; from++)
			{
			out <<= 1;
			out |= ((*(in+(from>>3)))>>(from&0x7))&1;
			}

		}
	return out;
	}

/*
Extra               Extra               Extra
Code Bits Length(s) Code Bits Lengths   Code Bits Length(s)
---- ---- ------     ---- ---- -------   ---- ---- -------
 257   0     3       267   1   15,16     277   4   67-82
 258   0     4       268   1   17,18     278   4   83-98
 259   0     5       269   2   19-22     279   4   99-114
 260   0     6       270   2   23-26     280   4  115-130
 261   0     7       271   2   27-30     281   5  131-162
 262   0     8       272   2   31-34     282   5  163-194
 263   0     9       273   3   35-42     283   5  195-226
 264   0    10       274   3   43-50     284   5  227-257
 265   1  11,12      275   3   51-58     285   0    258
 266   1  13,14      276   3   59-66
*/

typedef struct
{
    uint8_t len;
    uint8_t bits;
} size_tbl_t;

static size_tbl_t size_codes [20] = {
                        {11,1},{13,1},{15,1},{17,1},{19,2},{23,2},{27,2},{31,2},{35,3},{43,3},{51,3},{59,3},
                        {67,4},{83,4},{99,4},{115,4},{131,5},{163,5},{195,5},{227,5}
                        };

uint16_t get_size_from_code(uint8_t code, uint8_t* in, uint32_t* bit_pointer)
{
    if(code<=8)
        return code+2;

    if((code-8)>19)
        return 258;

    uint8_t out = size_codes[code-8].len;
    out+=get_bits((*bit_pointer)+size_codes[code-8].bits,(*bit_pointer),in);
     (*bit_pointer)+=size_codes[code-8].bits;
    return out;
}
/*
     Extra           Extra               Extra
Code Bits Dist  Code Bits   Dist     Code Bits Distance
---- ---- ----  ---- ----  ------    ---- ---- --------
  0   0    1     10   4     33-48    20    9   1025-1536
  1   0    2     11   4     49-64    21    9   1537-2048
  2   0    3     12   5     65-96    22   10   2049-3072
  3   0    4     13   5     97-128   23   10   3073-4096
  4   1   5,6    14   6    129-192   24   11   4097-6144
  5   1   7,8    15   6    193-256   25   11   6145-8192
  6   2   9-12   16   7    257-384   26   12  8193-12288
  7   2  13-16   17   7    385-512   27   12 12289-16384
  8   3  17-24   18   8    513-768   28   13 16385-24576
  9   3  25-32   19   8   769-1024   29   13 24577-32768
  */

typedef struct
{
 uint16_t dist;
 uint8_t bits;
} offset_tbl_t;

static offset_tbl_t offset_codes [31] = {
                                         {1,0},{2,0},{3,0},{4,0},{5,1},{7,1},{9,2},{13,2},
                                         {17,3},{25,3},{33,4},{49,4},{65,5},{97,5},{129,6},{193,6},
                                         {257,7},{385,7},{513,8},{769,8},{1025,9},{1537,9},{2049,10},{3073,10},
                                         {4097,11},{6145,11},{8193,12},{12289,12},{16385,13},{24577,13}
                                         };


uint16_t get_offset_from_code(uint8_t code, uint8_t* in, uint32_t* bit_pointer)
{
    uint16_t out = offset_codes[code].dist;
    out+=get_bits((*bit_pointer)+offset_codes[code].bits-1,(*bit_pointer),in);
    (*bit_pointer)+=offset_codes[code].bits;
    return out;
}

void inflate_static(uint8_t* in, uint32_t len)
	{
	uint32_t bit_pointer = 3;
	uint8_t i;
	for(i=0; i!=len; i++)
		{
		uint32_t data = get_bits(bit_pointer,bit_pointer+4,in);
		//256-259 - 		//280-287
		if((data<=0x5)||(data==0x18))
			{
			uint8_t code = get_bits(bit_pointer,bit_pointer+6,in);
			bit_pointer+=7;
            //256
            if(code == 0)
                break;

			uint16_t copy_size = get_size_from_code(code,in,&bit_pointer);

			code = get_bits(bit_pointer,bit_pointer+4,in);
			bit_pointer+=5;

			uint16_t copy_offset = get_offset_from_code(code,in,&bit_pointer);

			do
				{
				stack_push(stack[stack_pointer-copy_offset]);
				}
			while(--copy_size);
			}
		//0-143
		if((data<=0x17)&&(data>=0x6))
			{
            uint32_t data = get_bits(bit_pointer,bit_pointer+7,in);
            bit_pointer+=8;
            stack_push(data-0x30);
			}

        //144-255
		if(data>=0x19)
			{
            uint32_t data = get_bits(bit_pointer,bit_pointer+8,in);
            bit_pointer+=9;
            stack_push(data&0xff);
			}
		}
	}

int main ()
	{

	FILE * pFile;
	long lSize;
	char * buffer;
	size_t result;

	pFile = fopen ( "test.gz" , "rb" );
	if (pFile==NULL)
		{
		fputs ("File error",stderr);
		exit (1);
		}

	// obtain file size:
	fseek (pFile , 0 , SEEK_END);
	lSize = ftell (pFile);
	rewind (pFile);

	// allocate memory to contain the whole file:
	buffer = (char*) malloc (sizeof(char)*lSize);
	if (buffer == NULL)
		{
		fputs ("Memory error",stderr);
		exit (2);
		}

	// copy the file into the buffer:
	result = fread (buffer,1,lSize,pFile);
	if (result != lSize)
		{
		fputs ("Reading error",stderr);
		exit (3);
		}
	/*
	    png_out_t png_out =
	    {
	        .cb = lcd_write
	    };

	    png_parse_file((uint8_t*)buffer,lSize,&png_out);
	*/
	inflate_static((uint8_t*)buffer,lSize);

	fclose (pFile);
	free (buffer);
	return 0;
	}

