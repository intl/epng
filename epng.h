#include <stdint.h>
#include <stdio.h>

#define PNG_USE_PALETTE 0
#define PNG_USE_OUT_TO_RAM 0

#if(PNG_USE_OUT_TO_RAM)
#include <string.h>
#endif

#define PNG_CHUNK_IHDR_NAME 0x52444849 //"IHDR"
#define PNG_CHUNK_PLTE_NAME 0x45544c50 //"PLTE"
#define PNG_CHUNK_IDAT_NAME 0x54414449 //"IDAT"
#define PNG_CHUNK_IEND_NAME 0x444e4549//"IEND"

#define PNG_COLOR_TYPE_GRAYSCALE 0
#define PNG_COLOR_TYPE_RGB 2
#define PNG_COLOR_TYPE_PALETE 3
#define PNG_COLOR_TYPE_GREYSCALE_ALPHA 4
#define PNG_COLOR_TYPE_RGBA 6

#define PNG_FILE_SIGNATURE_0 0x474e5089//\89PNG
#define PNG_FILE_SIGNATURE_1 0xa1a0a0d //\r\n\1a\n

#define NTOHL(n) (((((uint32_t)(n) & 0xFF)) << 24) | \
                  ((((uint32_t)(n) & 0xFF00)) << 8) | \
                  ((((uint32_t)(n) & 0xFF0000)) >> 8) | \
                  ((((uint32_t)(n) & 0xFF000000)) >> 24))

#define NTOHS(n) (((((unsigned short)(n) & 0xFF)) << 8) | (((unsigned short)(n) & 0xFF00) >> 8))

#define PNG_DEFLATE_UNCOMPRESSED 0
#define PNG_DEFLATE_STATIC_HUFFMAN 1
#define PNG_DEFLATE_DYNAMIC_HUFFMAN 2
#define PNG_DEFLATE_ERROR_TYPE 3

#define PNG_FILTER_NONE 0
#define PNG_FILTER_SUB 1
#define PNG_FILTER_UP 2
#define PNG_FILTER_AVG 3
#define PNG_FILTER_PAETH 4

typedef struct
	{
	uint32_t signature[2];
	} png_file_header_t;

typedef struct
	{
	uint32_t chunk_len;
	uint32_t chunk_type;
	} png_chunk_t;

typedef struct
	{
	uint32_t width;
	uint32_t heigth;
	uint8_t bit_depth;
	uint8_t color_type;
	uint8_t compression_method;
	uint8_t filter_method;
	uint8_t interlace_method;
	uint32_t crc32;
	} png_IHDR_chunk_t;

typedef struct
	{
	uint8_t R;
	uint8_t G;
	uint8_t B;
	} png_color_t;

typedef enum
	{
	PNG_ERROR_NONE,
	PNG_ERROR_FILE_SIGNATURE,
	PNG_ERROR_EOF_NOT_FOUND,
	PNG_ERROR_DEFLATE_SIZE_TOO_SMALL,
	PNG_ERROR_DEFLATE_CONF_WRONG,
	PNG_ERROR_DEFLATE_UNCOMPRESSED_LEN_MISMATCH,
	PNG_ERROR_DEFLATE_UNKNOWN_TYPE,
	PNG_ERROR_FILTER_UNKNOWN_TYPE,
	PNG_ERROR_FILTER_UNKNOWN_COLOR_TYPE
	} png_error_t;

typedef struct
    {
        uint32_t heigth;
        uint32_t width;
        uint8_t bit_depth;
        uint8_t color_type;
        uint8_t bit_per_pixel;
#if(PNG_USE_PALETTE)
        png_color_t* plte;
        uint8_t plte_len;
#endif

#if(PNG_USE_OUT_TO_RAM)
        png_color_t *raw_data;
        uint32_t raw_data_len;
#else
        void (*cb)(png_color_t* color);
#endif
    } png_out_t;


typedef struct
	{
	union
		{
		struct
			{
			uint8_t cm:4;
			uint8_t cinfo:4;
			};
		uint8_t cmf;
		};

	union
		{
		struct
			{
			uint8_t fcheck:5;
			uint8_t fdict:1;
			uint8_t flevel:2;
			};
		uint8_t flg;
		};
	} png_zlib_conf_t;


png_error_t png_parse_file(uint8_t* in, uint32_t len,png_out_t* out);
png_error_t png_parse_idat_deflate(uint8_t* in, uint32_t len, png_out_t* out);
png_error_t png_parse_filtered_data(uint8_t* in, uint32_t len, png_out_t* out);
