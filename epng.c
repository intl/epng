#include <stdio.h>
#include "epng.h"

#if(PNG_USE_OUT_TO_RAM)
#include <string.h>
#include <stdlib.h>
#endif

/*
uint8_t read_bit(uint32_t* bit_pointer,uint8_t* in)
	{
	uint8_t out = (in[(*bit_pointer)>>3] >> ((*bit_pointer)&0x7))&1;
	(*bit_pointer)++;
	return out;
	}
*/

uint8_t paeth_predictor(uint16_t a, uint16_t b, uint16_t c) //Paeth predicter, used by PNG filter type 4
	{
	uint16_t p = a + b - c;

	uint16_t pa = 0;
	uint16_t pb = 0;
	uint16_t pc = 0;

	if(p>a)
		pa = p-a;
	else
		pa = a-p;

	if(p>b)
		pb = p-b;
	else
		pb = b-p;

	if(p>c)
		pc = p-c;
	else
		pc = c-p;

	if(pa <= pb && pa <= pc)
		return a;
	else if(pb <= pc)
		return b;
	else
		return c;
	}

png_error_t png_parse_filtered_data(uint8_t* in, uint32_t len, png_out_t* out)
	{
	uint8_t byte_per_pixel = (out->bit_per_pixel/8);
	uint32_t y=0;
	for(y=0; y<=out->heigth; y++)
		{
		uint32_t x = 0;
		len--;
		switch((*in++))
			{
			case PNG_FILTER_NONE:
				{
				for(x = 0 ; x<= out->width-1; x++)
					{
					switch(out->color_type)
						{
						case PNG_COLOR_TYPE_RGB:
						case PNG_COLOR_TYPE_RGBA:
							{
							png_color_t* raw_x = (png_color_t*)(in+(x*byte_per_pixel));
							out->cb(raw_x);
							}; break;
                        default:
                            {
                            return PNG_ERROR_FILTER_UNKNOWN_COLOR_TYPE;
                            }; break;
						}
					}
				in+=x*byte_per_pixel;
				len-=x*byte_per_pixel;
				};
			break;
			case PNG_FILTER_SUB:
				{
				///p[x+1][y] = p[x][y]+p[x+1][y];
				out->cb((png_color_t*)(in));
				for(x=1; x<= out->width-1; x++)
					{
					switch(out->color_type)
						{
						case PNG_COLOR_TYPE_RGB:
						case PNG_COLOR_TYPE_RGBA:
							{
							png_color_t* raw_x = (png_color_t*)(in+(x*byte_per_pixel));
							png_color_t* raw_x_bpp = (png_color_t*)(in+((x-1)*byte_per_pixel));

							raw_x->R = raw_x_bpp->R+raw_x->R;
							raw_x->G = raw_x_bpp->G+raw_x->G;
							raw_x->B = raw_x_bpp->B+raw_x->B;
							out->cb(raw_x);
							};
						break;
						default:
							{
							return PNG_ERROR_FILTER_UNKNOWN_COLOR_TYPE;
							};
						break;
						}
					}
				in+=x*byte_per_pixel;
				len-=x*byte_per_pixel;
				};
			break;
			case PNG_FILTER_UP:
				{
				///p[x][y] = p[x][y] + p[x][y-1];

				for(x = 0; x<= out->width-1; x++)
					{
					png_color_t* raw_x = (png_color_t*)(in+(x*byte_per_pixel));
					switch(out->color_type)
						{
						case PNG_COLOR_TYPE_RGB:
						case PNG_COLOR_TYPE_RGBA:
							{
							if(!y)
								{
								out->cb(raw_x);
								}
							else
								{
								/// Указатель на предыдушую строку = указатель на текущую (x) - (размер строки (width) * ,байт в пикселе) - 1 байт настроек
								png_color_t* prior_y = ((png_color_t*)(in-(byte_per_pixel*(out->width))+(x*byte_per_pixel)-1));
								raw_x->R = prior_y->R+raw_x->R;
								raw_x->G = prior_y->G+raw_x->G;
								raw_x->B = prior_y->B+raw_x->B;
								out->cb(raw_x);
								}
							};
						break;
						default:
							{
							return PNG_ERROR_FILTER_UNKNOWN_COLOR_TYPE;
							};
						break;
						}
					}
				in+=x*byte_per_pixel;
				len-=x*byte_per_pixel;
				};
			break;
			case PNG_FILTER_AVG:
				{
				for(x = 0; x<= out->width-1; x++)
					{
					png_color_t* raw_x = (png_color_t*)(in+(x*byte_per_pixel));
					switch(out->color_type)
						{
						case PNG_COLOR_TYPE_RGB:
						case PNG_COLOR_TYPE_RGBA:
							{
							if(!y)
								{
								if(!x)
									out->cb(raw_x);
								else
									{
									png_color_t* raw_x_bpp = (png_color_t*)(in+((x-1)*byte_per_pixel));
									raw_x->R = (raw_x_bpp->R/2)+raw_x->R;
									raw_x->G = (raw_x_bpp->G/2)+raw_x->G;
									raw_x->B = (raw_x_bpp->B/2)+raw_x->B;
									out->cb(raw_x);
									}
								}
							else
								{
								png_color_t* prior_y = ((png_color_t*)(in-(byte_per_pixel*(out->width))+(x*byte_per_pixel)-1));
								if(!x)
									{
									raw_x->R = (prior_y->R/2)+raw_x->R;
									raw_x->G = (prior_y->G/2)+raw_x->G;
									raw_x->B = (prior_y->B/2)+raw_x->B;
									out->cb(raw_x);
									}
								else
									{
									png_color_t* raw_x_bpp = (png_color_t*)(in+((x-1)*byte_per_pixel));
									raw_x->R = ((prior_y->R+raw_x_bpp->R)/2)+raw_x->R;
									raw_x->G = ((prior_y->G+raw_x_bpp->G)/2)+raw_x->G;
									raw_x->B = ((prior_y->B+raw_x_bpp->B)/2)+raw_x->B;
									out->cb(raw_x);
									}
								}
							};
						break;
						default:
							{
							return PNG_ERROR_FILTER_UNKNOWN_COLOR_TYPE;
							};
						break;
						}
					}
				in+=x*byte_per_pixel;
				len-=x*byte_per_pixel;
				};
			break;
			case PNG_FILTER_PAETH:
				{
				for(x = 0; x<= out->width-1; x++)
					{
					png_color_t* raw_x = (png_color_t*)(in+(x*byte_per_pixel));
					switch(out->color_type)
						{
						case PNG_COLOR_TYPE_RGB:
						case PNG_COLOR_TYPE_RGBA:
							{
							if(!y)
								{
								if(!x)
									out->cb(raw_x);
								else
									{
									png_color_t* raw_x_bpp = (png_color_t*)(in+((x-1)*byte_per_pixel));
									raw_x->R = paeth_predictor(raw_x_bpp->R,0,0) + raw_x->R; //(pre_in_x->R/2)+raw_x->R;
									raw_x->G = paeth_predictor(raw_x_bpp->G,0,0) + raw_x->G; //(pre_in_x->R/2)+raw_x->R;
									raw_x->B = paeth_predictor(raw_x_bpp->B,0,0) + raw_x->B; //(pre_in_x->R/2)+raw_x->R;
									out->cb(raw_x);
									}
								}
							else
								{
								png_color_t* prior_y = ((png_color_t*)(in-(byte_per_pixel*(out->width))+(x*byte_per_pixel)-1));
								if(!x)
									{

									raw_x->R = paeth_predictor(0,prior_y->R,0) + raw_x->R; //(pre_in_x->R/2)+raw_x->R;
									raw_x->G = paeth_predictor(0,prior_y->G,0) + raw_x->G; //(pre_in_x->R/2)+raw_x->R;
									raw_x->B = paeth_predictor(0,prior_y->B,0) + raw_x->B; //(pre_in_x->R/2)+raw_x->R;
									out->cb(raw_x);
									}
								else
									{
									png_color_t* prior_y_bpp = ((png_color_t*)(in-(byte_per_pixel*(out->width))+((x-1)*byte_per_pixel)-1));
									png_color_t* raw_x_bpp = (png_color_t*)(in+((x-1)*byte_per_pixel));

									raw_x->R = paeth_predictor(raw_x_bpp->R,prior_y->R,prior_y_bpp->R) + raw_x->R; //(pre_in_x->R/2)+raw_x->R;
									raw_x->G = paeth_predictor(raw_x_bpp->G,prior_y->G,prior_y_bpp->G) + raw_x->G; //(pre_in_x->R/2)+raw_x->R;
									raw_x->B = paeth_predictor(raw_x_bpp->B,prior_y->B,prior_y_bpp->B) + raw_x->B; //(pre_in_x->R/2)+raw_x->R;
									out->cb(raw_x);
									}
								}
							};
						break;

						default:
							{
							return PNG_ERROR_FILTER_UNKNOWN_COLOR_TYPE;
							};
						break;
						}
					}
				in+=x*byte_per_pixel;
				len-=x*byte_per_pixel;
				};
			break;
			default:
				{
				return PNG_ERROR_FILTER_UNKNOWN_TYPE;
				};
			break;
			}
		}
		return PNG_ERROR_NONE;
	}


png_error_t png_parse_idat_deflate(uint8_t* in, uint32_t len, png_out_t* out)
	{
	png_zlib_conf_t* deflate_conf = (png_zlib_conf_t*)in;
///Проверим размер, если размер меньше двух, тут делать нечего.
	if(len<2)
		{
		/*error, size of zlib data too small*/
		return PNG_ERROR_DEFLATE_SIZE_TOO_SMALL;
		}
///Для PNG указано, что тип может быть только 8
	if((deflate_conf->cm!=8)||(deflate_conf->cinfo>7)||(deflate_conf->fdict!=0))
		{
		/*error: only compression method 8: inflate with sliding window of 32k is supported by the PNG spec*/
		/*error: the specification of PNG says about the zlib stream:
		 "The additional flags shall not specify a preset dictionary."*/
		return PNG_ERROR_DEFLATE_CONF_WRONG;
		}

/// Сдвинем указатель памяти на 2; на начало блока deflate
	in+=2;
	len-=2;

	while(1)
		{
		typedef struct
			{
			uint8_t b_final:1;
			uint8_t b_type:2;
			uint8_t huffman_0:5;
			} png_deflate_start_block_conf_t;

		png_deflate_start_block_conf_t* start_block = (png_deflate_start_block_conf_t*) (in++);
		len--;

		switch(start_block->b_type)
			{
			case PNG_DEFLATE_UNCOMPRESSED:
				{
				typedef struct
					{
					uint16_t len;
					uint16_t nlen;
					} png_deflate_uncompresed_data_t;

				png_deflate_uncompresed_data_t* u_block = (png_deflate_uncompresed_data_t*) (in);

				in+=sizeof(png_deflate_uncompresed_data_t);
				len-=sizeof(png_deflate_uncompresed_data_t);

				///uncompressed len+nlen != 0xffff
				///uncompressed len != png_idat_len - sizeof(deflate_adler32_crc)
				if((u_block->len+u_block->nlen!=0xffff)||((u_block->len+4)!=len))
					{
					return PNG_ERROR_DEFLATE_UNCOMPRESSED_LEN_MISMATCH;
					}
				/// Почитать о фильтрах можно здесь http://www.w3.org/TR/PNG-Filters.html
				/// Примеры http://lodev.org/lodepng/picopng.cpp
				/// https://github.com/lvandeve/lodepng/blob/master/lodepng.cpp
                return png_parse_filtered_data(in,len,out);
				};
			break;
			case PNG_DEFLATE_STATIC_HUFFMAN:
				{
/*
Возьмем пример из http://compression.ru/download/articles/lz/mihalchik_deflate_decoding.html
http://www.compression-pointers.ru/compress_140.html


строка Deflate late в hex выглядит как: 0x44;0x65;0x66;0x6c;0x61;0x74;0x65;0x20;0x6c;0x61;0x74;0x65
В bin строка выглядит как 0b01000100;0b01100101;0b01100110;0b01101100;0b01110100;0b01100101;0b00100000;0b01101100;0b01100001;0b01110100;0b01100101

В запакованом static deflate строка выглядит как: 0x73;0x49;0x4d;0xcb;0x49;0x2c;0x49;0x55;0;0x11;0
и в бинарном запакованым: 0b01110011;0b01001001;0b01001101;0b11001011;0b01001001;0b00101100;0b01001001;0b01010101;0b00000000;0b00010001;0b00000000

Строка имеет битовый формат MSB->LSB;MSB->LSB (b[1] -> [7..0]; b[2] -> [15..8] ...)
Байтовый формат LSB->MSB

Берем первый байт:
бит 0 -> указывает последний ли это блок, если 1 то последний
биты 1-2 -> Указывают на тип компрессии, 0- статическая, 1 - динамическая
Биты 3-7 -> Указывают на код Хаффмана

Второй байт:
Бит 0-2 -> Указывает на продолжение кода Хаффмана

Для приведенного примера выше будет:
d[0] = 0b01110011 -> Последний блок, статическая компресия, Первая половина кода хаффмана(X[0]) 01110
d[1] = 0b01001001 -> Вторая половина кода Х (X[1]) = 001; Собирая в кучу код получаем 0b01110100 -> 0x74
Для цифр от 0 до 143 используются коды от 0b00110000-0b10111111 (0x30 - 0xbf); Для этих цифр поправка 0x30 (разница от 0 до начала); значит от 0x74-0x30 = 0x44;
Берем остаток от d[1] (без второй половины статического кода Х первого символа (т.е без первых трех бит)), он является X[0] - 0b10010
d[2] = 0b01001101 - из третьего байта берем первые 3 бита, получаем X[1] = 0b101, собираем все в кучу 0b10010101 = 0x95 -> отнимаем 0x30 = 0x65;
                    отсюда же берем X[0] для третьего распакованого байта -> 0b10010
d[3] = 0b11001011 -> Для третьего байта X[1] = 0b011; Собираем: 0b10010110 -> 0x96 < 0xbf -> 0x96-0x30 = 0x66;
                            X[0] для четвертого байта = 0x10011
d[4] = 0b01001001 -> X[1] для четвертого байта = 0x100 -> 0b10011100 ->0x30 < 0x9c < 0xbf -> 0x9c-0x30 = 0x6c;
                        X[0][5] = 0b10010;
d[5] = 0b00101100 -> X[1][5] = 0b001; -> 0b10010001 -> 0x30 < 0x91 < 0xbf -> 0x91-0x30 = 0x61;
                        X[0][6] = 0b10100;
d[6] = 0b01001001 -> X[1][6] = 0b100; -> 0b10100100 -> 0x30 < 0xa4 < 0xbf -> 0xA4 - 0x30 = 0x74;
                        X[0][7] = 0b10010;
d[7] = 0b01010101 -> X[1][7] = 0b101; -> 0b10010101 -> 0x30 < 0x95 < 0xbf -> 0x95 - 0x30 = 0x65;
                        X[0][8] = 0b01010;
d[8] = 0b00000000 -> X[1][8] = 0b000 -> 0b01010000 -> 0x30 < 0x50 < 0xbf -> 0x50 - 0x30 = 0x20;
                        X[0][9] = 0b00000

Вот тут начинается самое интересное.

d[9] = 0b00010001 -> X[1][9] = 0b100 -> 0x00000100 -> 0x30 > 0x04 -> Так как 0x4 меньше 0x30 то, значит это "управляющие" коды (7 битный код Хафманна),
                                                                    которые начинаются от 0 и до 0x30, И так как это данный код 7 битный, выполняется сдвиг на 1
                                                                    Итого код = 2;
                                                    -> 0x4 >>1 -> 0x2 -> код = 256+2 = 258, т.е размер одинаковых данных = 4, по таблице bp page 11 rfc1951
                                                    смещение в таком случае кодируется в следующих 5 битах + 1 или 2 бита(?) в 7 бите D[9]
                                                    Смещение = 0b00100 -> 4 + 1+d[9][7] = 5; Итого имеем размер копируемого блока = 4, смещение = 5;

typedef struct
{
huffman1:3;
huffman0:5;
} png_huffman_8bit_code_t;

uint8_t data = reverse_bits(start_block->huffman_0)<<3;
for(i=1;i!=width*heigth*byte_per_pixel;i++)
{
    data |= reverse_bits(((png_huffman_8bit_code_t*)in)->huffman1);
    if((data=>0x30)&&(data<0xc0)) //0-143
    {
        data -=0x30;
        stack_put(data);
    }

    if(data=>0xc0) // 144-255;
    {

    }

    if((data<0x30)&&(data)) 257-285
    {
        typedef struct
        {
        uint8_t base:2;
        uint8_t offset:5;
        uint8_t add_bit:1
        } png_huffman_7bit_code_t;
    uint8_t offset = ((png_huffman_7bit_code_t*)in)->offset + 1+ ((png_huffman_7bit_code_t*)in)->add_bit; //???
    uint8_t size = size_table(((png_huffman_7bit_code_t*)in)->base);

/// в этом месте кроется громадная жопа.
/// Есть три варианта решения проблемы.
/// Вариант 1, использовать ОЗУ. Что не есть решением
    for(i=0;i!=size;i++)
        stack_put(out[stack_pointer-offset]);
    }
/// Вариант 2. Использовать ОЗУ дисплея, что является решением, но корявым.
/// Вариант 3. Выполнять пересчет с минусом в указателе, и заново пересчитывать и выводить.
/// Смещение правдиво, но с учетом всех замен
/// deflate [5;4] -> deflate late
/// deflate [5;4] [10;4] -> deflate late late
///
    in++;
}
*/
				};
			break;
			case PNG_DEFLATE_DYNAMIC_HUFFMAN:
				{

				};
			break;
			default:
				{
				///несуществует типа deflate > 2
				return PNG_ERROR_DEFLATE_UNKNOWN_TYPE;
				};
			break;
			}
		if(start_block->b_final)
			break;
		}
	return PNG_ERROR_NONE;
	}

/**<
Функция разбора заголовка файла.
Если заголовок не равен PNG_FILE_SIGNATURE вернется ошибка PNG_ERROR_FILE_SIGNATURE
*/
png_error_t png_parse_file(uint8_t* in, uint32_t len,png_out_t* out)
	{
	png_file_header_t* file_header = (png_file_header_t*)in;

	if((file_header->signature[0]!=PNG_FILE_SIGNATURE_0)||(file_header->signature[1]!=PNG_FILE_SIGNATURE_1))
		{
		return PNG_ERROR_FILE_SIGNATURE;
		}
	in+=sizeof(png_file_header_t);

	uint8_t break_cycle = 0;
	while(!break_cycle)
		{
		png_chunk_t* png_chunk = (png_chunk_t*)in;
		switch(png_chunk->chunk_type)
			{
			case PNG_CHUNK_IHDR_NAME:
				{
				out->width = NTOHL(((png_IHDR_chunk_t*)(in+sizeof(png_chunk_t)))->width);
				out->heigth = NTOHL(((png_IHDR_chunk_t*)(in+sizeof(png_chunk_t)))->heigth);
				out->bit_depth = ((png_IHDR_chunk_t*)(in+sizeof(png_chunk_t)))->bit_depth;
				out->color_type = ((png_IHDR_chunk_t*)(in+sizeof(png_chunk_t)))->color_type;

				switch(out->color_type)
					{
					default:
						{
						out->bit_per_pixel = out->bit_depth;
						};
					break;
					case PNG_COLOR_TYPE_GREYSCALE_ALPHA:
					case PNG_COLOR_TYPE_RGBA:
						{
						out->bit_per_pixel = out->bit_depth*(out->color_type-2);
						};
					break;
					case PNG_COLOR_TYPE_RGB:
						{
						out->bit_per_pixel = out->bit_depth*3;
						};
					break;
					}

				};
			break;
			case PNG_CHUNK_PLTE_NAME:
				{
#if(PNG_USE_PALETTE)
				out->plte_len = png_chunk->chunk_len;
				out->plte = (png_color_t)(in+sizeof(png_chunk_t));
#endif
				};
			break;
			case PNG_CHUNK_IDAT_NAME:
				{
				png_parse_idat_deflate((in+sizeof(png_chunk_t)),NTOHL((uint32_t)png_chunk->chunk_len),out);
				};
			break;
			case PNG_CHUNK_IEND_NAME:
				{
				break_cycle = 1;
				};
			break;
			default:
				break;
			}
		//there might be test crc32
		// ---
		//
		in+=(NTOHL(png_chunk->chunk_len)+sizeof(png_chunk_t)+sizeof(uint32_t));
		len-=(NTOHL(png_chunk->chunk_len)+sizeof(png_chunk_t)+sizeof(uint32_t));
		}
	if(len!=0)
		return PNG_ERROR_EOF_NOT_FOUND;

	return PNG_ERROR_NONE;
	}
